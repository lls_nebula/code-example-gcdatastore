# GCloud datastore wrapper

# Description

GCloud datastore wrapper

# Pre-reqs
To build and run this app locally you will need a few things:
- Install [Node.js](https://nodejs.org/en/)

# Getting started
- Clone the repository
```
git clone git@bitbucket.org:realtimedataprotection/gcdatastore.git
```
- Install dependencies
```
cd gcdatastore
npm install
```
- Build the project
```
npm run build
```

- Run the tests
```
npm run test
```

- Compile the docs
```
npm run docs
```



