import { DatastoreKey } from "@google-cloud/datastore/entity";
export { DatastoreKey };

export interface IGDSInitOptions {
    apiEndpoint?: string;
    namespace?: string;
    projectId?: string;
    keyFilename?: string;
    credentials?: object;
}

export interface IGDSInternalEntityModel {
    key: DatastoreKey;
    data: any;
    excludeFromIndexes?: string[];
}

export interface IExternalEntityModel {
    key: IGDSKey;
    data: any;
}

export interface IGDSKey {
    kind: string;
    name: string | number;
    ancestorKind?: string;
    ancestorName?: string;
}

export type QueryFilterOperator = "<" | "<=" | "=" | ">=" | ">" | "includes";

export interface IGDSFilter {
    propertyName: string;
    comparator: QueryFilterOperator;
    value: string|number|boolean;
}

// documentsData: {[docId: string]: any}
export interface ICollectionData { // ICollectionData
    [path: string]: {[docId: string]: any};
}
