export * from "./models/datastore";
export * from "./providers/gdatastore";
import { firestore } from "@vaultspace/firebase-server";
import { GDataStore } from "./providers/gdatastore";
const gcdsInstance: GDataStore = new GDataStore(firestore);
export { gcdsInstance };