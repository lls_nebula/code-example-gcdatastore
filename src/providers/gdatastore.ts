import Datastore from "@google-cloud/datastore";
import {IGDSKey, ICollectionData, IExternalEntityModel, DatastoreKey, QueryFilterOperator, IGDSFilter } from "../models/datastore";
import * as admin from "firebase-admin";
import { PathElement } from "@google-cloud/datastore/entity";
export interface ICursor {
    cursor?: string;
    finished: boolean;
}


export interface ICursorBase {
    propertyName: string;
    propertyLastValue: string;
    direction: FirebaseFirestore.OrderByDirection;
}

export interface ICompoundCursor {
    now: number;
    internals: { [subQueryId: string]: ICursor };
}

export interface ICompoundResult {
    results: any[];
    cursor?: string;
    allFinished: boolean;
}

export type TGFSKey = IGDSKey | Array<string>;

export class GDataStore {
    private _firestore: admin.firestore.Firestore;
    public KEY: string = "_path";
    public NAME: string = "_name";

    constructor(firestoreInstance: admin.firestore.Firestore) {
        this._firestore = firestoreInstance;
    }

    private _keyToReference(key: TGFSKey): FirebaseFirestore.DocumentReference {
        let reference: FirebaseFirestore.DocumentReference = null;
        if (Array.isArray(key)) {
            if (key.length % 2 != 0) {
                throw {
                    error: true,
                    message: `Firestore Array Key's need to a set of CollectionId, DocumentId pairs`,
                    code: 10165
                };
            }

            reference = this._firestore.collection(key[0]).doc(key[1]);

            for (let i = 2; i < key.length; i = i + 2) {
                reference = reference.collection(key[i]).doc(key[i + 1]);
            }
        } else {
            if (key.ancestorKind) {
                reference = this._firestore
                    .collection(key.ancestorKind).doc(key.ancestorName.toString())
                    .collection(key.kind).doc(key.name.toString());
            } else {
                reference = this._firestore
                    .collection(key.kind).doc(key.name.toString());
            }
        }

        return reference;
    }

    async createOne(key: TGFSKey, data: any, options?: FirebaseFirestore.SetOptions): Promise<FirebaseFirestore.WriteResult> {
        const reference: FirebaseFirestore.DocumentReference = this._keyToReference(key);
        const name: string = Array.isArray(key) ? key[key.length - 1] : key.name.toString();
        data[this.NAME] = name;

        return reference.set(data, options);
    }

    private _TGFSKeyToArrayPath(key: TGFSKey): Array<string> {
        if (Array.isArray(key)) return key;
        else {
            if (key.ancestorKind && key.ancestorName) {
                return [key.ancestorKind, key.ancestorName, key.kind, key.name.toString()];
            } else {
                return [key.kind, key.name.toString()];
            }
        }
    }

    public getCollectionOrDocumentRefFromPath(path: string): FirebaseFirestore.CollectionReference | FirebaseFirestore.DocumentReference {
        const pathSplit = path.split("/");
        let collectionOrDocRef: FirebaseFirestore.CollectionReference | FirebaseFirestore.DocumentReference = null;
        pathSplit.forEach((element: string, index: number) => {
            if (index % 2) {
                // even
                collectionOrDocRef = (<FirebaseFirestore.CollectionReference>collectionOrDocRef).doc(element);
            }
            else {
                // odd
                if (index == 0) {
                    collectionOrDocRef = this._firestore.collection(element);
                }
                else {
                    collectionOrDocRef = (<FirebaseFirestore.DocumentReference>collectionOrDocRef).collection(element);
                }
            }
        });

        return collectionOrDocRef;
    }

    public async downloadCollectionWithWildCard(collectionPath: string, dataBin?: any) {
        return this.iterateCollection(null, null, collectionPath.split("/"), dataBin || {});
    }


    public async downloadCollectionsWithWildCard(collectionPaths: Array<string>) {
        const outputData = {};
        let resolver = Promise.resolve(null);
        collectionPaths.forEach((path: string) => {
            resolver = resolver.then(async (it) => {
                await this.downloadCollectionWithWildCard(path, outputData);
                return null;
            });
        });
        await resolver;

        return outputData;
    }

    public async saveCollectionsData(collectionsData: any) {
        return Object.keys(collectionsData).map((collectionFullPath: string) => {
            const fullRef = <FirebaseFirestore.DocumentReference> this.getCollectionOrDocumentRefFromPath(collectionFullPath);
            fullRef.set(collectionsData[collectionFullPath]);
        });
    }

    public async iterateCollection(sourceRef: FirebaseFirestore.CollectionReference | FirebaseFirestore.DocumentReference, _sourcePath: Array<string>, remainingCollectionPath: Array<string>, dataBin: any) {
        console.log("remainingCollectionPath", remainingCollectionPath);
        const firstPair = remainingCollectionPath.slice(0, 2);
        const remainingPairs = remainingCollectionPath.slice(2, remainingCollectionPath.length);
        const sourcePath = _sourcePath || [];
        console.log("sourceRef", sourceRef, firstPair[0]);
        if (!sourceRef) {
            sourceRef = this.getCollectionOrDocumentRefFromPath(firstPair[0]);
        } else {
            sourceRef = (<FirebaseFirestore.DocumentReference> sourceRef).collection(firstPair[0]);
        }

        if (firstPair[1] === "*") {
            // iter all collection
            const colRef = await (<FirebaseFirestore.CollectionReference> sourceRef).get();
            colRef.docs.forEach(async (docRef: FirebaseFirestore.QueryDocumentSnapshot) => {
                const id = docRef.id;
                const data = docRef.data();
                const fullPath = `${(sourcePath || []).concat(firstPair[0]).concat(id).join("/")}`;
                console.log("fullPath wildcard", fullPath, sourcePath, firstPair);
                dataBin[fullPath] = data;
                // sourceRef, _sourcePath = sourcePath.concat(firstPair), remaining =
            });

            for (let i = 0; i < colRef.docs.length - 1; i++) {
                if (remainingPairs.length) {
                    await this.iterateCollection(colRef.docs[i].ref, sourcePath.concat([firstPair[0], colRef.docs[i].id]), remainingPairs, dataBin);
                }
            }
        } else {
            // get doc
            const docRef = (<FirebaseFirestore.CollectionReference> sourceRef).doc(firstPair[1]);
            const fullPath = `${(sourcePath || []).concat(firstPair[1]).concat(firstPair[1]).join("/")}`;
            console.log("fullPath non wildcard", fullPath);

            const docSnap = await docRef.get();
            dataBin[fullPath] = docSnap.data();

            if (remainingPairs.length) {
                await this.iterateCollection(sourceRef, sourcePath.concat(firstPair), remainingPairs, dataBin);
            }
        }

        return dataBin;
    }

    async downloadCollection(collectionPath: string): Promise<{[docId: string]: any}> {
        // path like /Applications
        // /Application/
        const pathSplit = collectionPath.split("/");
        if (!(pathSplit.length % 2)) {
            throw `Collection path must have an odd number`;
        }
        // if collection does not have wildcard
        if (collectionPath.includes("*")) {
            console.log("collection includes wildcard", collectionPath);
            // Application/*/Product
            const firstWildCard = pathSplit.findIndex((splitVal: string) => {
                return splitVal == "*";
            });

            console.log("firstWildCard", firstWildCard);

            if (firstWildCard != 0) {
                // get path fragment
                const firstPath: Array<string> = [];

                for (let i = 0; i < firstWildCard; i++) {
                    firstPath.push(pathSplit[i]);
                }

                const fragment = firstPath.join("/");
                console.log("fragment", fragment);
                const pathRef = this.getCollectionOrDocumentRefFromPath(fragment);
                console.log("pathRef", pathRef);

                for (let i = firstWildCard; i < (pathSplit.length - 1); i++) {
                    const current = pathSplit[i];
                    if (current === "*") {
                        // iterate docs
                        const collectionGet = pathRef.get();
                    } else {
                        // extend normal
                    }
                }
            } else {
                throw "Wildcard * cannot be used as a collection";
            }
        } else {
            const collectionRef: FirebaseFirestore.CollectionReference = <FirebaseFirestore.CollectionReference>this.getCollectionOrDocumentRefFromPath(collectionPath);
            const documents = await collectionRef.get();
            const data: {[docId: string]: any} = {};
            documents.forEach(async (doc) => {
                data[doc.id] = doc.data();
            });

            return data;
        }

    }

    async downloadCollections(collectionPaths: Array<string>): Promise<{[collectionPath: string]: {[docId: string]: any}}> {
        const data: {[collectionPath: string]: {[docId: string]: any}} = {};
        await Promise.all(collectionPaths.map(async (path: string) => {
            const collectionData = await this.downloadCollection(path);
            data[path] = collectionData;
        }));
        return data;
    }

    async uploadCollection(collectionPath: string, documentsData: {[docId: string]: any}): Promise<void> {
        const pathSplit = collectionPath.split("/");
        if (!(pathSplit.length % 2)) {
            throw `Collection path must have an odd number`;
        }

        if (!documentsData) {
            throw `No document data provided`;
        }

        if (!Object.keys(documentsData).length) {
            throw `Document data was invalid`;
        }

        const collectionRef: FirebaseFirestore.CollectionReference = <FirebaseFirestore.CollectionReference>this.getCollectionOrDocumentRefFromPath(collectionPath);
        const promises: Array<Promise<any>> = [];

        Object.keys(documentsData).forEach((docId: string) => {
            promises.push(collectionRef.doc(docId).set(documentsData[docId]));
        });

        await Promise.all(promises);
    }

    async uploadCollections(collectionsBackup: ICollectionData): Promise<void> {
        await Promise.all(Object.keys(collectionsBackup).map((collectionPath: string) => {
            return this.uploadCollection(collectionPath, collectionsBackup[collectionPath]);
        }));
    }

    async getOne(key: TGFSKey, hideKey?: boolean): Promise<any> {
        const snapshot: FirebaseFirestore.DocumentSnapshot = await this._keyToReference(key).get();
        const data: any = await snapshot.data();

        if (!data) return null;
        if (!hideKey) { data[this.KEY] = this._TGFSKeyToArrayPath(key); }

        return data;
    }

    private _QueryFilterOperator2WhereFilterOp(comparator: QueryFilterOperator): FirebaseFirestore.WhereFilterOp {
        let whereOp: FirebaseFirestore.WhereFilterOp = null;
        if (comparator == "<") {
            whereOp = "<";
        } else if (comparator == "<=") {
            whereOp = "<=";
        } else if (comparator == "=") {
            whereOp = "==";
        } else if (comparator == ">") {
            whereOp = ">";
        } else if (comparator == ">=") {
            whereOp = ">=";
        } else if (comparator == "includes") {
            whereOp = "array-contains";
        }

        return whereOp;
    }

    private _getFiltersKindInfoQuery(kind: string, filters: IGDSFilter[], ancestorKind?: string, ancestorKey?: string, order?: { [fieldName: string]: { descending: boolean } }, limit?: number, cursor?: any, nativeCursor?: any): Promise<FirebaseFirestore.QuerySnapshot> {
        let reference: FirebaseFirestore.CollectionReference = null;
        let query: FirebaseFirestore.Query = null;
        if (ancestorKind && ancestorKey) {
            reference = this._firestore.collection(ancestorKind).doc(ancestorKey).collection(kind);
        } else {
            reference = this._firestore.collection(kind);
        }

        filters.forEach((filter) => {
            query = (query || reference).where(
                filter.propertyName,
                this._QueryFilterOperator2WhereFilterOp(filter.comparator),
                filter.value
            );
        });

        const _cursor = !!cursor ? this._cursorDecoder(cursor) : null;

        if (order) {
            Object.keys(order).forEach((orderPropertyName: string) => {
                const direction: FirebaseFirestore.OrderByDirection = order[orderPropertyName].descending == true ? "desc" : "asc";
                query = (query || reference).orderBy(orderPropertyName, direction);
            });
        } else {
            if (_cursor) {
                query = (query || reference).orderBy(_cursor.propertyName, _cursor.direction);
            }
        }

        if (limit) {
            query = (query || reference).limit(limit);
        }

        if (cursor) {
            if (_cursor.propertyLastValue) {
                query = (query || reference).startAfter(nativeCursor || _cursor.propertyLastValue);

            }
        }

        return query == null ? reference.get() : query.get();
    }

    async queryMany(kind: string, filters: IGDSFilter[], ancestorKind?: string, ancestorKey?: string, hideKey?: boolean): Promise<any[]> {
        const dataSnapshot: FirebaseFirestore.QuerySnapshot = await this._getFiltersKindInfoQuery(kind, filters, ancestorKind, ancestorKey);
        const returnData: any = [];
        dataSnapshot.forEach((result: FirebaseFirestore.QueryDocumentSnapshot) => {
            const data: any = result.data();
            const key: TGFSKey = [];

            if (ancestorKind && ancestorKey) {
                key.push(ancestorKind);
                key.push(ancestorKey);
            }

            key.push(kind);
            key.push(result.id);
            if (!hideKey) { data[this.KEY] = key; }
            returnData.push(data);
        });

        return returnData;
    }

    async deleteOne(key: DatastoreKey): Promise<FirebaseFirestore.WriteResult> {
        const kind: string = key.kind;
        const path: Array<string> = key.path.map((pathElement: PathElement) => pathElement.toString());
        const reference: FirebaseFirestore.DocumentReference = this._keyToReference(path);

        return reference.delete();
    }

    async createMany(entities: IExternalEntityModel[]): Promise<FirebaseFirestore.WriteResult[]> {
        const batch: FirebaseFirestore.WriteBatch = this._firestore.batch();
        entities.forEach((externalEntity: IExternalEntityModel) => {
            const normKey: Array<string> = this._TGFSKeyToArrayPath(externalEntity.key);
            const reference = this._keyToReference(normKey);
            externalEntity.data[this.NAME] = externalEntity.key.name;
            batch.create(reference, externalEntity.data);
        });

        return batch.commit();
    }

    async deleteMany(keys: DatastoreKey[]): Promise<FirebaseFirestore.WriteResult[]> {
        const batch: FirebaseFirestore.WriteBatch = this._firestore.batch();

        keys.forEach((key: DatastoreKey) => {
            const path: Array<string> = key.path.map((pathElement: PathElement) => pathElement.toString());
            const reference: FirebaseFirestore.DocumentReference = this._keyToReference(path);
            batch.delete(reference);
        });

        return batch.commit();
    }

    async updateMany(entities: IExternalEntityModel[]): Promise<FirebaseFirestore.WriteResult[]> {
        const batch: FirebaseFirestore.WriteBatch = this._firestore.batch();

        entities.forEach((externalEntity: IExternalEntityModel) => {
            const normKey: Array<string> = this._TGFSKeyToArrayPath(externalEntity.key);
            const reference = this._keyToReference(normKey);
            batch.update(reference, externalEntity.data);
        });

        return batch.commit();
    }

    async updateOne(key: IGDSKey, data: any): Promise<FirebaseFirestore.WriteResult> {
        let _key: Array<string> = [];

        if (key.ancestorName) {
            _key = [key.ancestorKind, key.ancestorName, key.kind, key.name.toString()];
        } else {
            _key = [key.kind, key.name.toString()];
        }

        const reference = this._keyToReference(_key);

        return reference.update(data);
    }

    public _cursorEncoder(data: ICursorBase | any): string {
        return Buffer.from(JSON.stringify(data)).toString("base64");
    }

    public _cursorDecoder(data: string | any): any {
        return JSON.parse(Buffer.from(data, "base64").toString("ascii"));
    }

    async queryManyLimited(
        kind: string,
        filters: IGDSFilter[] = [],
        limit = 1000,
        _cursor?: string | null,
        ancestorKind?: string | null,
        ancestorKey?: string | null,
        order?: { [fieldName: string]: { descending: boolean } },
        hideKey?: boolean
    ): Promise<ICompoundResult> {
        const preferedFilter = filters.find((filter: IGDSFilter) => {
            if (filter.comparator == "<=" || filter.comparator == "<" || filter.comparator == ">=" || filter.comparator == ">") return true;
        });

        let direction: FirebaseFirestore.OrderByDirection = "asc";

        if (preferedFilter) {
            if ((preferedFilter.comparator == "<" || preferedFilter.comparator == "<=")) {
                direction = "desc";
            }
            else if (preferedFilter.comparator == ">" || preferedFilter.comparator == ">=") {
                direction = "asc";
            }
            else {
                direction = "asc";
            }
        }

        let cursor = null;

        if (_cursor == null && preferedFilter) {
            cursor = this._cursorEncoder({ propertyName: preferedFilter.propertyName, direction: direction });
        } else {
            cursor = _cursor;
        }

        const dataSnapshot: FirebaseFirestore.QuerySnapshot = await this._getFiltersKindInfoQuery(kind, filters, ancestorKind, ancestorKey, order, limit, cursor);
        const returnData: any = [];

        dataSnapshot.forEach((result: FirebaseFirestore.QueryDocumentSnapshot) => {
            const data: any = result.data();
            const key: TGFSKey = [];

            if (ancestorKind && ancestorKey) {
                key.push(ancestorKind);
                key.push(ancestorKey);
            }

            key.push(kind);
            key.push(result.id);

            if (!hideKey) { data[this.KEY] = key; }

            returnData.push(data);
        });


        let cursorOut: any = null;
        if (order) {
            const fieldName = Object.keys(order)[0];

            if (returnData.length) {
                cursorOut = this._cursorEncoder(
                    {
                        propertyName: fieldName,
                        propertyLastValue: returnData[returnData.length - 1][fieldName],
                        direction: order[fieldName].descending == true ? "desc" : "asc"
                    }
                );
            }
        } else {
            let fieldName = null;

            if (filters.length && returnData.length && preferedFilter) {
                fieldName = preferedFilter.propertyName;
                cursorOut = this._cursorEncoder(
                    {
                        propertyName: fieldName,
                        propertyLastValue: returnData[returnData.length - 1][fieldName],
                        direction: direction
                    }
                );
            } else if (returnData.length) {
                cursorOut = this._cursorEncoder({
                    propertyName: "_name",
                    propertyLastValue: returnData[returnData.length - 1]._name,
                    direction: "asc"
                });
            }
        }

        let hasNext: boolean = true;

        if (!returnData.length) {
            hasNext = false;
        } else if (returnData.length < limit) {
            hasNext = false;
        } else {
            const nextData: any = [];
            const hasNextSnapShot = await this._getFiltersKindInfoQuery(
                kind,
                filters,
                ancestorKind,
                ancestorKey,
                order,
                1,
                cursorOut
            );

            hasNextSnapShot.forEach((result: FirebaseFirestore.QueryDocumentSnapshot) => {
                const data: any = result.data();
                const key: TGFSKey = [];

                if (ancestorKind && ancestorKey) {
                    key.push(ancestorKind);
                    key.push(ancestorKey);
                }

                key.push(kind);
                key.push(result.id);

                if (!hideKey) { data[this.KEY] = key; }

                nextData.push(data);
            });

            if (!nextData.length) {
                hasNext = false;
            } else {
                const d = !!cursorOut ? this._cursorDecoder(cursorOut) : null;
                hasNext = !!hasNextSnapShot.size;
                if (d) {
                    if (d.direction == "asc") {
                      // @TODO
                    } else {
                        if (d.propertyLastValue < nextData[0][d.propertyName]) {
                            hasNext = false;
                        }
                    }
                }
            }
        }

        const returnObj: ICompoundResult = {
            results: returnData,
            allFinished: !hasNext ? true : false,
        };

        if (cursorOut && hasNext) {
            returnObj.cursor = cursorOut;
        }
        return returnObj;
    }

    private makeId(object: any) {
        return JSON.stringify(object);
    }

    async compoundQueryMany(kind: string, listOfFilterLists: IGDSFilter[][], ancestorKind?: string, ancestorKey?: string, limit: number = 100, compoundCursorString?: string, hideKey?: boolean): Promise<ICompoundResult> {

        // go through each sub query 0 -> n
        // calculate offset from each sub query e.g. if limit if 100 and we have 99 entities
        // then start searching from second subQuery from start or cursor and limit 1

        // build compound object
        // ICompoundCursor
        const compoundCursor = !!compoundCursorString ? this._cursorDecoder(compoundCursorString) : null;
        let compountInternal: ICompoundCursor = null;
        if (compoundCursor) {
          compountInternal = compoundCursor;
        } else {
            compountInternal = { internals: {}, now: Date.now() };
            listOfFilterLists.forEach((filterList: IGDSFilter[]) => {
                const id: string = this.makeId(filterList);
                compountInternal.internals[id] = { finished: false };
            });
        }

        // build query descriptions
        const queries: any = {};
        const remainingSubQueries = listOfFilterLists.forEach((filterList: IGDSFilter[]) => {
            const id: string = this.makeId(filterList);
            const relevantCursorObj = compountInternal.internals[id];
            if (relevantCursorObj) {
                if (relevantCursorObj.finished != true) {
                    queries[id] = { filterList: filterList, startCursor: relevantCursorObj.cursor, finished: false };
                } else {
                    queries[id] = { filterList: filterList, startCursor: null, finished: true };
                }
            } else {
                throw { error: true, message: `Invalid compound cursor parsed to compoundQueryMany`, code: 28462 };
            }

        });

        const limitClone: number = limit;
        let remainingOffset: number = limitClone;
        let resolver = Promise.resolve();

        let resultSet: any[] = [];

        Object.keys(queries).forEach((queryId: string) => {
            resolver = resolver.then(async () => {
                const queryDescription = queries[queryId];

                // skip if finished
                if (queryDescription.finished == true || remainingOffset == 0) {
                    return;
                }

                const subQueryResult = await this.queryManyLimited(
                    kind,
                    queryDescription.filterList,
                    remainingOffset,
                    queryDescription.startCursor,
                    ancestorKind, ancestorKey,
                    undefined,
                    hideKey
                );

                const data = subQueryResult.results;
                resultSet = resultSet.concat(data);
                remainingOffset -= data.length;
                if (!subQueryResult.allFinished) {
                    compountInternal.internals[queryId].finished = false;
                    compountInternal.internals[queryId].cursor = subQueryResult.cursor;
                } else {
                    compountInternal.internals[queryId].finished = true;
                    delete compountInternal.internals[queryId]["cursor"];
                }
            });
        });

        return resolver.then(() => {
            const allFinished: boolean = Object.keys(compountInternal.internals).every((key: string) => {
                const compoundObject = compountInternal.internals[key];

                return compoundObject.finished == true;
            });

            const resultObject: ICompoundResult = { results: resultSet, allFinished: allFinished };

            if (!allFinished) {
                resultObject.cursor = this._cursorEncoder(compountInternal);
            }

            return resultObject;
        });

    }
}
