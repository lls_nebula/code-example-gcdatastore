import { GDataStore, ICompoundResult } from "../src/providers/gdatastore";
import { firestore } from "@vaultspace/firebase-server";

import {
    IGDSFilter,
} from "../src/models/datastore";
import { TIMEOUT } from "dns";

const chai = require("chai");
const expect = chai.expect;
function getRandomInt(min: any, max: any) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function addYearsToDate(someDate: Date, years: number = 1) {
    const expireDate: Date = new Date(someDate);
    expireDate.setUTCFullYear(expireDate.getUTCFullYear() + years);
    return expireDate;
}


describe("GDataStore SERVICE", () => {
    let gdatastore: GDataStore;

    const ancestorCleanUp: any = [];
    const descendantCleanUp: any = [];
    const testAncestorKind = "FIRESTORETEST_AncestorKind";
    const testAncestorEntityName = "FIRESTORETEST_AncestorEntityName_";
    const getAncestorEntityName = () => { const id = `${testAncestorEntityName}${Math.random()}`; ancestorCleanUp.push(id); return id; };
    const descendantKind = "FIRESTORETEST_DescendantKind";
    const descendantEntityName = "FIRESTORETEST_DescendantEntityName_";
    const getdescendantEntityName = () => { const id = `${descendantEntityName}${Math.random()}`; descendantCleanUp.push(id); return id; };


    beforeEach(() => {
        gdatastore = new GDataStore(firestore);
    });

    it("should query all within a complex compound query", async () => {
        const now = new Date();

        // do query test
        let allDone: boolean = false;
        let resultSet: any[] = [];
        let oldCursor: string = null;
        const filters: IGDSFilter[][] = [ // OR FILTERS
            [ // AND FILTERS
                { propertyName: "policy", comparator: "=", value: true },
                { propertyName: "startDate", comparator: ">", value: <any>addYearsToDate(now, -1).getTime() },
                { propertyName: "startDate", comparator: "<", value: <any>now.getTime() },
                { propertyName: "cancelled", comparator: "=", value: false },
                { propertyName: "numberOfMonthUnitsForTerm", comparator: "=", value: 12 }
            ],
        ];

        // get all and count

        const fullResults = await gdatastore.queryMany("QuotePolicy", filters[0]);
        const fullCount = fullResults.length;
        console.log("filters", filters);
        while (allDone == false) {
            console.log("CURRENT CURSOR", oldCursor);
            const getPage: ICompoundResult = await gdatastore.compoundQueryMany("QuotePolicy", filters, null, null, 13, oldCursor);
            oldCursor = getPage.cursor;
            allDone = getPage.allFinished;
            resultSet = resultSet.concat(getPage.results);
            console.log("PAGE RESULT X", getPage.results.length, getPage.cursor, getPage.allFinished);
        }

        expect(resultSet.length).to.equal(fullCount);

    });

});



