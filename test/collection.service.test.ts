const chai = require("chai");
const expect = chai.expect;
import { firestore } from "@vaultspace/firebase-server";
import { GDataStore, ICompoundCursor, ICompoundResult } from "../src/providers/gdatastore";
import fs from "fs";

describe("GDataStore SERVICE", () => {
    let gdatastore: GDataStore;

    beforeEach(() => {
        gdatastore = new GDataStore(firestore);
    });

     it("Should download collections", async() => {
       const collectionsToDownload = [
        "Customer/*",
        "Contact/*",
        "Application/*/Product/*",
        "Application/*/Reports/*",
        "Application/*/EmailContent/*",
        "Application/*/ApplicationFunnel/*",
        "QuotePolicy/*/PaymentPlanInstallments/*",
        "EventContextStream/*",
        "EntityCollectionDelta/*",
        "EventStreamComputedFields/*"
       ];
       const collectionsData = await gdatastore.downloadCollectionsWithWildCard(collectionsToDownload);
       console.log("-------------------------------#");
       console.log(JSON.stringify(collectionsData, null, 4));
       const now = Date.now();
       fs.writeFileSync(`./test/outputData/wildcard_${now}`, JSON.stringify(collectionsData, null, 4), {encoding: "utf8"});
     });
});
