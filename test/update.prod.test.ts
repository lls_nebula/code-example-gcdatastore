const chai = require("chai");
const expect = chai.expect;
import { firestore } from "@vaultspace/firebase-server";
import { GDataStore } from "../src/providers/gdatastore";
import { ICollectionData } from "../src/models/datastore";
import fs from "fs";

describe("GDataStore SERVICE", () => {
    let gdatastore: GDataStore;

    beforeEach(() => {
        gdatastore = new GDataStore(firestore);
    });
     // uploadCollections
     it("Should upload collections data", async function () {
         const data: ICollectionData = JSON.parse(fs.readFileSync("./test/outputData/1561565138084", "utf8"));
         await gdatastore.uploadCollections(data);
     });
});
