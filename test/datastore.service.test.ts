import { GDataStore, ICompoundResult } from "../src/providers/gdatastore";
import { firestore } from "@vaultspace/firebase-server";

import {
  IGDSKey,
  IGDSFilter,
  IExternalEntityModel,
  DatastoreKey
} from "../src/models/datastore";
import { TIMEOUT } from "dns";

const chai = require("chai");
const expect = chai.expect;
function getRandomInt(min: any, max: any) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}
describe("GDataStore SERVICE", () => {
  let gdatastore: GDataStore;

  const ancestorCleanUp: any = [];
  const descendantCleanUp: any = [];
  const testAncestorKind = "FIRESTORETEST_AncestorKind";
  const testAncestorEntityName = "FIRESTORETEST_AncestorEntityName_";
  const getAncestorEntityName = () => { const id = `${testAncestorEntityName}${Math.random()}`; ancestorCleanUp.push(id); return id; };
  const descendantKind = "FIRESTORETEST_DescendantKind";
  const descendantEntityName = "FIRESTORETEST_DescendantEntityName_";
  const getdescendantEntityName = () => { const id = `${descendantEntityName}${Math.random()}`; descendantCleanUp.push(id); return id; };


  beforeEach(() => {
    gdatastore = new GDataStore(firestore);
  });


 it("Should create entity", async function () {
    this.timeout(50000);
    const testKey: IGDSKey = {kind: testAncestorKind, "name": getAncestorEntityName() };
    const testCreateOne = {"number": 0, "string": "string", "date": new Date()};
    const result = await gdatastore.createOne(testKey, testCreateOne);
    expect(result.writeTime.seconds).to.be.an("number");
  });

  it("Should create descendant entity", async function () {
    this.timeout(50000);
    const newAncestorEntId: string = getAncestorEntityName();
    const newDescendantEntId: string = getdescendantEntityName();

    const testKey: IGDSKey = {kind: testAncestorKind, "name": newAncestorEntId };
    const testCreateOne: any = {"number": 0, "string": "string", "date": (new Date().getTime())};
    const result = await gdatastore.createOne(testKey, testCreateOne);
    expect(result.writeTime.seconds).to.be.an("number");

    const testDescendantKey: IGDSKey = {ancestorKind: testAncestorKind, ancestorName: newAncestorEntId, kind: descendantKind, name: newDescendantEntId};
    const descendantResult = await gdatastore.createOne(testDescendantKey, {"descNumber": 0, "descString": "saddasdas"});
    expect(descendantResult.writeTime.seconds).to.be.an("number");
  });

  it("Should fetch entity", async () => {
    const testKey: IGDSKey = {kind: testAncestorKind, "name": ancestorCleanUp[0] };
    const testCreateOne: any = {"number": 0, "string": "string", "date": (new Date().getTime())};
    const createResult = await gdatastore.createOne(testKey, testCreateOne);
    expect(createResult.writeTime.seconds).to.be.an("number");

    const result = await gdatastore.getOne(testKey);
    expect(result).to.be.an("object");
    Object.keys(testCreateOne).forEach((fieldName: string) => {
      expect(testCreateOne[fieldName]).to.equal(result[fieldName]);
    });

  });


  it("Should fetch descendant entity including array style", async function () {

    this.timeout(50000);
    const newAncestorEntId: string = getAncestorEntityName();
    const newDescendantEntId: string = getdescendantEntityName();

    const testKey: IGDSKey = {kind: testAncestorKind, "name": newAncestorEntId };
    const testCreateOne: any = {"number": 0, "string": "string", "date": (new Date().getTime())};
    const result = await gdatastore.createOne(testKey, testCreateOne);
    expect(result.writeTime.seconds).to.be.an("number");

    const testDescendantKey: IGDSKey = {ancestorKind: testAncestorKind, ancestorName: newAncestorEntId, kind: descendantKind, name: newDescendantEntId};
    const descendantData: any = {"descNumber": 0, "descString": "saddasdas"};
    const descendantResult = await gdatastore.createOne(testDescendantKey, descendantData);
    expect(descendantResult.writeTime.seconds).to.be.an("number");

    this.timeout(30000);
    let queryResult = await gdatastore.getOne(testDescendantKey);
    console.log("Result", JSON.stringify(queryResult, null, 4));
    expect(queryResult).to.be.an("object");
    Object.keys(descendantData).forEach((fieldName: string) => {
      expect(descendantData[fieldName]).to.equal(queryResult[fieldName]);
    });

    queryResult = await gdatastore.getOne([testAncestorKind, newAncestorEntId, descendantKind,  newDescendantEntId]);
    console.log("Result KEY STYLE", JSON.stringify(queryResult, null, 4));
    expect(queryResult).to.be.an("object");
    Object.keys(descendantData).forEach((fieldName: string) => {
      expect(descendantData[fieldName]).to.equal(queryResult[fieldName]);
    });
  });

  it("Should not fetch no existing entity", async function () {
    this.timeout(30000);
    const testKey: IGDSKey = {kind: testAncestorKind, "name": "someIdWhichDoesNotExist" };
    const result = await gdatastore.getOne(testKey);
    chai.assert.equal(result, null);
  });

  it("Should not fetch no descendant existing entity", async function () {
    this.timeout(30000);
    const newAncestorEntId: string = `TestAncestorEnt1_${Math.random()}`;
    const newDescendantEntId: string = `TestDescendantEnt1_${Math.random()}`;

    const testKey: IGDSKey = {kind: descendantKind, "name": "asaads", ancestorKind: testAncestorKind, ancestorName: "somekeywhichdoesnotexist"};
    const result = await gdatastore.getOne(testKey);
    chai.assert.equal(result, null);
  });

  it("Should query existing entities with string within their properties", function (done) {
    this.timeout(30000);
    const filters: IGDSFilter[] = [{propertyName: "string", comparator: "=", value: "string" }];
    gdatastore.queryMany(testAncestorKind, filters).then((result) => {
      console.log("Result", result);
      chai.assert.notEqual(result, null);
      chai.assert.notEqual(result.length, 0);
      done();
    }).catch(done);
  });

  it("Should query existing entities without filters", function (done) {
    this.timeout(30000);
    const filters: IGDSFilter[] = [];
    gdatastore.queryMany(testAncestorKind, filters).then((result) => {
      console.log("Result", result);
      chai.assert.notEqual(result, null);
      chai.assert.notEqual(result.length, 0);
      done();
    }).catch(done);
  });

  it ("Should delete a single entity from the database", async function (done) {
    this.timeout(50000);

    // create a test entity
    const entName: string = getAncestorEntityName();
    const testKey: IGDSKey = {kind: testAncestorKind, "name": entName };
    const testCreateOne = {"number": 0, "string": "string", "date": new Date()};
    gdatastore.createOne(testKey, testCreateOne).then((createResult) => {
      console.log(`New entity was created:`, createResult);

      // remove the entity that was fetched
      const ent = { kind: testAncestorKind, path: [testAncestorKind, entName]};
      gdatastore.deleteOne(ent).then((deleteResult) => {
        console.log(`Entity was deleted`, deleteResult);

        // try to fetch it again
        gdatastore.getOne(testKey).then((fetchResult) => {
          console.log(`Entity was fetched:`, JSON.stringify(fetchResult));
          chai.assert.equal(fetchResult, null);
        }).catch(done);
      }).catch(done);
    }).catch(done);

    done();
  });

  it("Should create many", async function () {
    this.timeout(50000);

    const entropy: string = `${Math.random()}`;
    const testKeyOne: IGDSKey = {kind: testAncestorKind , "name": getAncestorEntityName() };
    const testCreateOne = {"number": 0, "string": entropy, "date": new Date()};
    const testKeyTwo: IGDSKey = {kind: testAncestorKind, "name": getAncestorEntityName() };
    const testCreateTwo = {"number": 0, "string": entropy, "date": new Date()};
    const results = await gdatastore.createMany(
      [
        {key: testKeyOne, data: testCreateOne},
        {key: testKeyTwo, data: testCreateTwo}
      ]
    );
    // query
    const queryResults = await gdatastore.queryMany(testAncestorKind, [{propertyName: "string", comparator: "=", value: entropy}]);
    chai.assert.equal(queryResults.length, 2);
    // remove
    const keyRemoval = [
      { kind: testAncestorKind, path: [testAncestorKind, testKeyOne.name]},
      { kind: testAncestorKind, path: [testAncestorKind, testKeyTwo.name]}
    ];
    await gdatastore.deleteMany(keyRemoval);
    // query
    const queryResultsPostDelete = await gdatastore.queryMany(testAncestorKind, [{propertyName: "string", comparator: "=", value: entropy}]);
    chai.assert.equal(queryResultsPostDelete.length, 0);
    return;
  });

  it ("Should delete a bulk of entities from the database", async function () {
    this.timeout(50000);

    // create a test entity
    const name = getAncestorEntityName();
    const testKey: IGDSKey = {kind: testAncestorKind, "name": name };
    const testCreateOne = {"number": 0, "string": "string", "date": new Date()};

    const createOneResult = await  gdatastore.createOne(testKey, testCreateOne);
    const ent = { kind: testAncestorKind, path: [testAncestorKind, name]};
    const deleteResult = await gdatastore.deleteMany([ent]);
    const getOneAgainResult = await gdatastore.getOne(testKey);
    chai.assert.equal(getOneAgainResult, null);
    return;
  });

  it("Should update many", async function () {
    this.timeout(50000);

    const entropy: string = `${Math.random()}`;
    const testKeyOne: IGDSKey = {kind: testAncestorKind, "name": getAncestorEntityName() };
    const testCreateOne = {"number": 0, "string": entropy, "date": new Date().getTime()};
    const testKeyTwo: IGDSKey = {kind: testAncestorKind, "name": getAncestorEntityName() };
    const testCreateTwo = {"number": 0, "string": entropy, "date": new Date().getTime()};
    const results = await gdatastore.createMany(
      [
        {key: testKeyOne, data: testCreateOne},
        {key: testKeyTwo, data: testCreateTwo}
      ]
    );
    // update
    const updateValues: IExternalEntityModel[] = [
      {key: testKeyOne, data: {"number": 1}},
      {key: testKeyTwo, data: {"number": 1}},
    ];
    const updateResults = await gdatastore.updateMany(updateValues);

    // query
    const queryResults = await gdatastore.queryMany(testAncestorKind, [{propertyName: "string", comparator: "=", value: entropy}]);
    chai.assert.equal(queryResults.length, 2);
    chai.assert.equal(queryResults[0].number, 1);
    chai.assert.equal(queryResults[1].number, 1);

    // remove
    const keyRemoval = [
      { kind: testAncestorKind, path: [testAncestorKind, testKeyOne.name]},
      { kind: testAncestorKind, path: [testAncestorKind, testKeyTwo.name]}
    ];
    await gdatastore.deleteMany(keyRemoval);
    // query
    const queryResultsPostDelete = await gdatastore.queryMany(testAncestorKind, [{propertyName: "string", comparator: "=", value: entropy}]);
    chai.assert.equal(queryResultsPostDelete.length, 0);
    return;
  });

  it("Should update one entity", async function () {
    this.timeout(50000);
    const testKey: IGDSKey = {kind: testAncestorKind, "name": getAncestorEntityName() };
    const testCreateOne = {"number": 0, "string": "string", "date": new Date()};
    const result = await gdatastore.createOne(testKey, testCreateOne);
    expect(result.writeTime.seconds).to.be.an("number");
    const updateResult = await gdatastore.updateOne(testKey, {"number": 1, "string": "string", "date": new Date()});
    const getResult = await gdatastore.getOne(testKey);
    expect(getResult.number).to.equal(1);
  });


  it("queryManyLimited method testing NO FILTER", async function () {
    const testKind = "queryManyLimited";
    const testDataLength = 7;
    const limit = 2;

    const getTestData = (count: number) => {
      return {
        key: {kind: testKind, name: count.toString()},
        data: {count},
      };
    };

    const arr: Array<{key: IGDSKey, data: {count: number}}> = [];
    const keyRemoval: Array<{kind: string, path: [string, string]}> = [];
    for (let i = 0; i < testDataLength; i++) {
      arr.push(getTestData(i));
      keyRemoval.push({kind: testKind, path: [testKind, i.toString()]});
    }

    try {
        await gdatastore.deleteMany(keyRemoval);
    }
    catch (e) {}

    await gdatastore.createMany(arr);

    let results = 0;
    let finished = false;
    let cursor = "";

    console.log("filters", []);
    const zeroth =  await gdatastore.queryMany(testKind, []);
    console.log("zeroth", zeroth);

    const firstAnswer = await gdatastore.queryManyLimited(testKind, [], limit);
    console.log("FIRST", firstAnswer);
    results += firstAnswer.results.length;
    cursor = firstAnswer.cursor;
    finished = firstAnswer.allFinished;

    while (!finished) {
      const answer = await gdatastore.queryManyLimited(testKind, [], limit, cursor);
      results += answer.results.length;
      cursor = answer.cursor;
      if (!cursor) { finished = true; }
    }

    console.log("keyRemoval", keyRemoval);
    await gdatastore.deleteMany(keyRemoval);

    console.log(results, testDataLength);
    expect(results === testDataLength).to.be.true;

  });


  it("queryManyLimited method testing WITH FILTER", async function () {
    const testKind = "queryManyLimited";
    const testDataLength = 7;
    const limit = 2;

    const pageFrom = getRandomInt(1, testDataLength - 1);
    const shouldFind = testDataLength - pageFrom;
    console.log("#######", testDataLength, pageFrom, shouldFind);

    const getTestData = (count: number) => {
      return {
        key: {kind: testKind, name: count.toString()},
        data: {count},
      };
    };

    const arr: Array<{key: IGDSKey, data: {count: number}}> = [];
    const keyRemoval: Array<{kind: string, path: [string, string]}> = [];
    for (let i = 0; i < testDataLength; i++) {
      arr.push(getTestData(i));
      keyRemoval.push({kind: testKind, path: [testKind, i.toString()]});
    }

    try {
        await gdatastore.deleteMany(keyRemoval);
    }
    catch (e) {}

    await gdatastore.createMany(arr);

    let results = 0;
    let finished = false;
    let cursor = "";

    console.log("filters",  [{propertyName: "count", comparator: ">=", value: pageFrom}]);

    const zeroth =  await gdatastore.queryMany(testKind, [{propertyName: "count", comparator: ">=", value: pageFrom}]);
    console.log("zeroth", zeroth);

    const firstAnswer = await gdatastore.queryManyLimited(testKind, [{propertyName: "count", comparator: ">=", value: pageFrom}], limit);
    console.log("FIRST", firstAnswer);
    results += firstAnswer.results.length;
    cursor = firstAnswer.cursor;
    finished = firstAnswer.allFinished;

    while (!finished) {
      const answer = await gdatastore.queryManyLimited(testKind, [{propertyName: "count", comparator: ">=", value: pageFrom}], limit, cursor);
      console.log("answer", answer);
      results += answer.results.length;
      cursor = answer.cursor;
      if (!cursor) { finished = true; }
    }

    console.log("keyRemoval", keyRemoval);
    await gdatastore.deleteMany(keyRemoval);

    console.log(results, shouldFind, testDataLength);
    expect((results) === shouldFind).to.be.true;

  });

  it("queryManyLimited method testing WITH MULTI FILTER 1", async function () {
    const testKind = "queryManyLimited";
    const testDataLength = 7;
    const limit = 2;

    const pageFrom = getRandomInt(1, testDataLength - 1);
    const shouldFind = testDataLength - pageFrom;
    console.log("#######", testDataLength, pageFrom, shouldFind);

    const getTestData = (count: number) => {
      return {
        key: {kind: testKind, name: count.toString()},
        data: {count, value: true},
      };
    };

    const arr: Array<{key: IGDSKey, data: {count: number}}> = [];
    const keyRemoval: Array<{kind: string, path: [string, string]}> = [];
    for (let i = 0; i < testDataLength; i++) {
      arr.push(getTestData(i));
      keyRemoval.push({kind: testKind, path: [testKind, i.toString()]});
    }

    try {
        await gdatastore.deleteMany(keyRemoval);
    }
    catch (e) {}

    await gdatastore.createMany(arr);

    let results = 0;
    let finished = false;
    let cursor = "";
    const filters: IGDSFilter[] = [
      {propertyName: "count", comparator: ">=", value: pageFrom},
      {propertyName: "value", comparator: "=", value: true},
    ];

    console.log("filters", filters);

    const zeroth =  await gdatastore.queryMany(testKind, filters);
    console.log("zeroth", zeroth);

    const firstAnswer = await gdatastore.queryManyLimited(testKind, filters, limit);
    console.log("FIRST", firstAnswer);
    results += firstAnswer.results.length;
    cursor = firstAnswer.cursor;
    finished = firstAnswer.allFinished;

    while (!finished) {
      const answer = await gdatastore.queryManyLimited(testKind, filters, limit, cursor);
      console.log("answer", answer);
      results += answer.results.length;
      cursor = answer.cursor;
      if (!cursor) { finished = true; }
    }

    console.log("keyRemoval", keyRemoval);
    await gdatastore.deleteMany(keyRemoval);

    console.log(results, shouldFind, testDataLength);
    expect((results) === shouldFind).to.be.true;

  });

  it("queryManyLimited method testing WITH MULTI FILTER 2", async function () {
    const testKind = "queryManyLimited";
    const testDataLength = 7;
    const limit = 2;

    const pageFrom = getRandomInt(1, testDataLength - 1);
    const shouldFind = testDataLength - pageFrom;
    console.log("#######", testDataLength, pageFrom, shouldFind);

    const getTestData = (count: number) => {
      return {
        key: {kind: testKind, name: count.toString()},
        data: {count, value: true},
      };
    };

    const arr: Array<{key: IGDSKey, data: {count: number}}> = [];
    const keyRemoval: Array<{kind: string, path: [string, string]}> = [];
    for (let i = 0; i < testDataLength; i++) {
      arr.push(getTestData(i));
      keyRemoval.push({kind: testKind, path: [testKind, i.toString()]});
    }

    try {
        await gdatastore.deleteMany(keyRemoval);
    }
    catch (e) {}

    await gdatastore.createMany(arr);

    let results = 0;
    let finished = false;
    let cursor = "";
    const filters: IGDSFilter[] = [
      {propertyName: "value", comparator: "=", value: true},
      {propertyName: "count", comparator: ">=", value: pageFrom},
    ];

    console.log("filters", filters);

    const zeroth =  await gdatastore.queryMany(testKind, filters);
    console.log("zeroth", zeroth);

    const firstAnswer = await gdatastore.queryManyLimited(testKind, filters, limit);
    console.log("FIRST", firstAnswer);
    results += firstAnswer.results.length;
    cursor = firstAnswer.cursor;
    finished = firstAnswer.allFinished;

    while (!finished) {
      const answer = await gdatastore.queryManyLimited(testKind, filters, limit, cursor);
      console.log("answer", answer);
      results += answer.results.length;
      cursor = answer.cursor;
      if (!cursor) { finished = true; }
    }

    console.log("keyRemoval", keyRemoval);
    await gdatastore.deleteMany(keyRemoval);

    console.log(results, shouldFind, testDataLength);
    expect((results) === shouldFind).to.be.true;

  });


  it("queryManyLimited method testing WITH MULTI FILTER LT 1", async function () {
    const testKind = "queryManyLimited";
    const testDataLength = 7;
    const limit = 2;

    const pageFrom = getRandomInt(0, testDataLength);
    const shouldFind = pageFrom;
    console.log("#######", testDataLength, pageFrom, shouldFind);

    const getTestData = (count: number) => {
      return {
        key: {kind: testKind, name: count.toString()},
        data: {count, value: true},
      };
    };

    const arr: Array<{key: IGDSKey, data: {count: number}}> = [];
    const keyRemoval: Array<{kind: string, path: [string, string]}> = [];
    for (let i = 0; i < testDataLength; i++) {
      arr.push(getTestData(i));
      keyRemoval.push({kind: testKind, path: [testKind, i.toString()]});
    }

    try {
        await gdatastore.deleteMany(keyRemoval);
    }
    catch (e) {}

    await gdatastore.createMany(arr);

    let results = 0;
    let finished = false;
    let cursor = "";
    const filters: IGDSFilter[] = [
      {propertyName: "value", comparator: "=", value: true},
      {propertyName: "count", comparator: "<", value: pageFrom},
    ];

    console.log("filters", filters);

    const zeroth =  await gdatastore.queryMany(testKind, filters);
    console.log("zeroth", zeroth);
    const resultsBin: any[] = [];

    const firstAnswer = await gdatastore.queryManyLimited(testKind, filters, limit);
    firstAnswer.results.forEach((result: any) => resultsBin.push(result));
    finished = firstAnswer.allFinished;
    console.log("ANSWER", firstAnswer);

    results += firstAnswer.results.length;
    cursor = firstAnswer.cursor;

    while (!finished) {
      const answer = await gdatastore.queryManyLimited(testKind, filters, limit, cursor);
      console.log("ANSWER", answer);
      results += answer.results.length;
      answer.results.forEach((result: any) => resultsBin.push(result));
      cursor = answer.cursor;
      if (!cursor) { finished = true; }
    }

    console.log("keyRemoval", keyRemoval);
    await gdatastore.deleteMany(keyRemoval);

    console.log(resultsBin.length, shouldFind, testDataLength);

    console.log(resultsBin);
    expect((results) === shouldFind).to.be.true;

  });

  it("queryManyLimited method testing WITH MULTI FILTER LT 2", async function () {
    const testKind = "queryManyLimited";
    const testDataLength = 7;
    const limit = 2;

    const pageFrom = getRandomInt(1, testDataLength - 1);
    const shouldFind = pageFrom + 1;
    console.log("#######", testDataLength, pageFrom, shouldFind);

    const getTestData = (count: number) => {
      return {
        key: {kind: testKind, name: count.toString()},
        data: {count, value: true},
      };
    };

    const arr: Array<{key: IGDSKey, data: {count: number}}> = [];
    const keyRemoval: Array<{kind: string, path: [string, string]}> = [];
    for (let i = 0; i < testDataLength; i++) {
      arr.push(getTestData(i));
      keyRemoval.push({kind: testKind, path: [testKind, i.toString()]});
    }

    try {
        await gdatastore.deleteMany(keyRemoval);
    }
    catch (e) {}

    await gdatastore.createMany(arr);

    let results = 0;
    let finished = false;
    let cursor = "";
    const filters: IGDSFilter[] = [
      {propertyName: "count", comparator: "<=", value: pageFrom},
      {propertyName: "value", comparator: "=", value: true},
    ];

    console.log("filters", filters);

    const zeroth =  await gdatastore.queryMany(testKind, filters);
    console.log("zeroth", zeroth);

    const firstAnswer = await gdatastore.queryManyLimited(testKind, filters, limit);
    console.log("FIRST", firstAnswer);
    results += firstAnswer.results.length;
    cursor = firstAnswer.cursor;
    finished = firstAnswer.allFinished;

    while (!finished) {
      const answer = await gdatastore.queryManyLimited(testKind, filters, limit, cursor);
      console.log("answer", answer);
      results += answer.results.length;
      cursor = answer.cursor;
      if (!cursor) { finished = true; }
    }

    console.log("keyRemoval", keyRemoval);
    await gdatastore.deleteMany(keyRemoval);

    console.log(results, shouldFind, testDataLength);
    expect((results) === shouldFind).to.be.true;

  });

  it("should query all within compound query", async () => {
    // create 400 entities with 4 different application names
    const numberOfEntitites = 100;
    const dataToAdd: IExternalEntityModel[] = [];
    let mod = -1;
    const allEntityNames = [...Array(numberOfEntitites).keys()];
    allEntityNames.forEach((index: number) => {
      if (index % 4 == 0) {
        mod = 0;
      }
      else {
        mod++;
      }
      dataToAdd.push({key: {kind: testAncestorKind, name: `${index}`}, data: {application: mod, name: index}});
    });

    try {
      await gdatastore.deleteMany(dataToAdd.map((it: IExternalEntityModel) => {
        const dsk: DatastoreKey = {path: [it.key.kind, it.key.name], kind: it.key.kind};
        return dsk;
      }));
    }
    catch (e) {}

    console.log("creating lots", dataToAdd.length);
    await gdatastore.createMany(dataToAdd);

    // wait due to eventual consistancy
    await new Promise((res) => setTimeout(res, 10000));

    // do query test
    let allDone: boolean = false;
    let resultSet: any[] = [];
    let oldCursor: string = null;
    const filters: IGDSFilter[][] = [
      [
        {propertyName: "application", comparator: "=", value: 0}
      ],
      [
        {propertyName: "application", comparator: "=", value: 1}
      ],
      [
        {propertyName: "application", comparator: "=", value: 2}
      ],
      [
        {propertyName: "application", comparator: "=", value: 3}
      ]
    ];
    console.log("filters", filters);
    while (allDone == false) {
      const getPage: ICompoundResult = await gdatastore.compoundQueryMany(testAncestorKind, filters, null, null, 13, oldCursor);
      oldCursor = getPage.cursor;
      allDone = getPage.allFinished;
      resultSet = resultSet.concat(getPage.results);
      console.log("PAGE RESULT X", getPage.results.length, getPage.cursor, getPage.allFinished);
    }

    expect(resultSet.length).to.equal(numberOfEntitites);
    resultSet.forEach((result) => {
      console.log("result", result.name, result.application);
      expect((<any> allEntityNames).includes(result.name)).to.equal(true);
    });
    console.log("--000000000000000000000");
    const mappedDedupedEntitie = Object.keys(resultSet.map((it) => it.name).reduce((acc, it) => {
      acc[it] = true;
      return acc;
    }, {}));
    console.log("mappedDedupedEntitie", JSON.stringify(mappedDedupedEntitie));
    expect(resultSet.length).to.equal(numberOfEntitites);


    // CLEANUP

    console.log("query all");
    let addedDataFromSystem = await gdatastore.queryMany(testAncestorKind, []);
    console.log("addedDataFromSystem.length", addedDataFromSystem.length);
    // remove all entities
    // delete
    console.log("removing many");
    await gdatastore.deleteMany(addedDataFromSystem.map((it) => {
      const key = it[gdatastore.KEY];
      console.log("IIIIITTT", it, key);
      return { kind: testAncestorKind, path: key};
    }));
    // test removed ok
    addedDataFromSystem = await gdatastore.queryMany(testAncestorKind, []);
    console.log("addedDataFromSystem.length", addedDataFromSystem.length);

    console.log("finished");
  });

  it("queryManyLimited method testing WITH ORDER", async function () {
    const testDataLength = 7;
    const limit = 2;

    const getTestData = (count: number) => {
      return {
        key: {kind: testAncestorKind, name: count.toString()},
        data: {count},
      };
    };

    const arr: Array<{key: IGDSKey, data: {count: number}}> = [];
    const keyRemoval: Array<{kind: string, path: [string, string]}> = [];
    for (let i = 0; i < testDataLength; i++) {
      arr.push(getTestData(i));
      keyRemoval.push({kind: testAncestorKind, path: [testAncestorKind, i.toString()]});
    }

    try {
      await gdatastore.deleteMany(keyRemoval.map((it: {kind: string, path: [string, string]}) => {
        const dsk: DatastoreKey = {path: it.path, kind: it.kind};
        return dsk;
      }));
    }
    catch (e) {}

    await gdatastore.createMany(arr);

    const descAnswer = await gdatastore.queryManyLimited(testAncestorKind, [], limit, undefined, undefined, undefined, {"count": {descending: true}});

    console.log("descAnswer", descAnswer);

    const ascAnswer = await gdatastore.queryManyLimited(testAncestorKind, [], limit, undefined, undefined, undefined, {"count": {descending: false}});

    console.log("ascAnswer", ascAnswer);

    expect(descAnswer.results[0].count > ascAnswer.results[0].count).to.equal(true);

    await gdatastore.deleteMany(keyRemoval);

  });


});
