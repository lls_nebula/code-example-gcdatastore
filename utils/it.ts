import { GDataStore } from "../src/providers/gdatastore";

import testCredentials from "../config/gdatastore.json";

const gdatastore: GDataStore = new GDataStore({projectId: testCredentials.project_id, credentials: testCredentials});

gdatastore.queryMany("Customer", []).then(async (policies) => {
  console.log("TOTAL Customer", policies.length);
  const mod500 = policies.length % 500;

  for (let i = 0; i < mod500; i++) {
    const reducedPolicies = policies.slice(i * 500, i * 500 + 500);
    console.log("REMOVING Customer", reducedPolicies.length);

    await gdatastore.deleteMany(reducedPolicies.map((it) => {
      const key = it[gdatastore.KEY];
      return { kind: key.kind, path: key.path};
    }));
  }
});
